#
# GitLab CI for Debian Java packaging
#

FROM debian:stable
MAINTAINER mvglasow <michael@vonglasow.com>

# Prepare System
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get --quiet update --yes
RUN apt-get --quiet install --yes build-essential devscripts debhelper default-jre-headless maven gradle
